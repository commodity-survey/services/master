`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate, commit} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { surveyor: () => ({}), approval: () => ({}), visitor: () => ({}) },
    Mutation: { survey: () => ({}) },
    SurveyForVisitorQuery: {
        get: async (__, req, ctx) => {
            let survey = await db.survey.findAll({
                attributes: [
                    'id', 'commodity_id', 'commodity_price', 'survey_date', 'approved',
                    [sequelize.col('mcom.commodity_name'), 'commodity_name'], 
                    [sequelize.col('mcom.unit_id'), 'unit_id'], 
                    [sequelize.col('mcom.category_id'), 'category_id'], 
                    [sequelize.col('mcom->mu.unit_name'), 'unit_name'], 
                    [sequelize.col('mcom->mc.category_name'), 'category_name']
                ],
                include : [
                    { 
                        model : db.m_commodity, as:'mcom', attributes : [], require : true,
                        include: [
                            { model : db.m_category, as:'mc', attributes : [], require : true },
                            { model : db.m_unit, as:'mu', attributes : [], require : true }
                        ]
                    },
                ],
                where: {
                    ... req.filter_date_min || req.filter_date_max ? { 
                        survey_date: { 
                            ... req.filter_date_min ? { [Op.gte]: new Date(req.filter_date_min) } : {},
                            ... req.filter_date_max ? { [Op.lte]: new Date(req.filter_date_max) } : {}
                        } 
                    } : {},
                    approved: 1, statusid: 1 
                },
                raw: true
            })
            survey = hashids.encodeArray(survey, ['id', 'commodity_id', 'category_id', 'unit_id'])
            return survey
        }
    },
    SurveyorQuery: {
        getForSurveyor: async (__, req, ctx) => {
            req.commodity = hashids.decodeArray(req.commodity)
            req.category = hashids.decodeArray(req.category)
            
            let survey = await db.survey.findAll({
                attributes: [
                    'id', 'commodity_id', 'commodity_price', 'survey_date', 'approved',
                    [sequelize.col('mcom.commodity_name'), 'commodity_name'], 
                    [sequelize.col('mcom.unit_id'), 'unit_id'], 
                    [sequelize.col('mcom.category_id'), 'category_id'], 
                    [sequelize.col('mcom->mu.unit_name'), 'unit_name'], 
                    [sequelize.col('mcom->mc.category_name'), 'category_name']
                ],
                include : [
                    { 
                        model : db.m_commodity, as:'mcom', attributes : [], require : true,
                        where : { ... req.category && req.category.length ? { category_id: { [Op.in]: req.category } } : {} },
                        include: [
                            { model : db.m_category, as:'mc', attributes : [], require : true },
                            { model : db.m_unit, as:'mu', attributes : [], require : true }
                        ]
                    },
                ],
                where: {
                    surveyor_id: ctx.req.auth.username,
                    ... req.filter_date_min || req.filter_date_max ? { 
                        survey_date: { 
                            ... req.filter_date_min ? { [Op.gte]: new Date(req.filter_date_min) } : {},
                            ... req.filter_date_max ? { [Op.lte]: new Date(req.filter_date_max) } : {}
                        } 
                    } : {},
                    ... req.commodity && req.commodity.length ? { commodity_id: { [Op.in]: req.commodity } } : {},
                    statusid: 1 
                },
                raw: true
            })
            survey = hashids.encodeArray(survey, ['id', 'commodity_id', 'category_id', 'unit_id'])
            return survey
        },
    },
    SurveyorApprovalQuery: {
        getForApproval: async (__, req, ctx) => {
            let getSurvey = (approvalStatus) => {
                return db.survey.findAll({
                    attributes: [
                        'id', 'commodity_price', 'survey_date', 'approved',
                        [sequelize.col('mcom.commodity_name'), 'commodity_name'],
                        [sequelize.col('mcom->mu.unit_name'), 'unit_name'], 
                        [sequelize.col('mcom->mc.category_name'), 'category_name']
                    ],
                    include : [
                        { 
                            model : db.m_commodity, as:'mcom', attributes : [], require : true,
                            include: [
                                { model : db.m_category, as:'mc', attributes : [], require : true },
                                { model : db.m_unit, as:'mu', attributes : [], require : true }
                            ]
                        },
                    ],
                    where: { approved: approvalStatus, statusid: 1 },
                    raw: true
                })
            }
            let survey = { waiting: await getSurvey(0), approved: await getSurvey(1), rejected: await getSurvey(2) }
            survey.waiting = hashids.encodeArray(survey.waiting, ['id'])
            survey.approved = hashids.encodeArray(survey.approved, ['id'])
            survey.rejected = hashids.encodeArray(survey.rejected, ['id'])
            return survey
        }
    },
    SurveyMutation: {
        create: async (__, req, ctx) => {
            let validator = new v(req, { 
                commodity_id: 'required', 
                survey_date: 'required|date|dateBeforeToday:-1,days', 
                commodity_price: 'required|numeric' 
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.commodity_id = hashids.decode(req.commodity_id)
            let validateCommodity = await db.m_commodity.findOne({
                attributes: ['id'],
                where: { id: req.commodity_id, statusid: 1 },
                raw: true
            })
            await validate(validateCommodity, 'user-input')

            let save = await db.survey.create({
                surveyor_id: ctx.req.auth.username,
                commodity_id: req.commodity_id,
                survey_date: req.survey_date,
                commodity_price: req.commodity_price,
                approved: 0,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async(__, req, ctx) => {
            let validator = new v(req, { 
                id: 'required', 
                commodity_id: 'required', 
                survey_date: 'required|date|dateBeforeToday:-1,days', 
                commodity_price: 'required|numeric' 
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.id = hashids.decode(req.id)
            let findSurvey = await db.survey.findOne({
                attributes: ['id'],
                where: { id: req.id, approved: 0, surveyor_id: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(findSurvey, 'user-input')

            req.commodity_id = hashids.decode(req.commodity_id)
            let validateCommodity = await db.m_commodity.findOne({
                attributes: ['id'],
                where: { id: req.commodity_id, statusid: 1 },
                raw: true
            })
            await validate(validateCommodity, 'user-input')

            let save = await sequelize.transaction()
            let deactivate = await db.survey.update(
                { statusid: 0 },
                { where: { id: findSurvey.id, statusid: 1 } }
            )
            let create = await db.survey.create({
                surveyor_id: ctx.req.auth.username,
                commodity_id: req.commodity_id,
                survey_date: req.survey_date,
                commodity_price: req.commodity_price,
                approved: 0,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            
            await commit(save, deactivate && create)
            return {status: 1}
        },
        delete: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.id = hashids.decode(req.id)
            let findSurvey = await db.survey.findOne({
                attributes: ['id', 'commodity_id', 'commodity_price', 'survey_date'],
                where: { id: req.id, approved: 0, surveyor_id: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(findSurvey, 'user-input')
            
            let save = await sequelize.transaction()
            let deactivate = await db.survey.update(
                { statusid: 0 },
                { where: { id: findSurvey.id, statusid: 1 } }
            )
            let create = await db.survey.create({
                surveyor_id: ctx.req.auth.username,
                commodity_id: findSurvey.commodity_id,
                survey_date: findSurvey.survey_date,
                commodity_price: findSurvey.commodity_price,
                approved: 0,
                create_by: ctx.req.auth.username,
                statusid: 0
            })
            
            await commit(save, deactivate && create)
            return {status: 1}
        },
        approve: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.id = hashids.decode(req.id)
            let findSurvey = await db.survey.findOne({
                attributes: ['id', 'commodity_id', 'commodity_price', 'survey_date'],
                where: { id: req.id, approved: 0, surveyor_id: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(findSurvey, 'user-input')
            
            let save = await sequelize.transaction()
            let deactivate = await db.survey.update(
                { statusid: 0 },
                { where: { id: findSurvey.id, statusid: 1 } }
            )
            let create = await db.survey.create({
                surveyor_id: ctx.req.auth.username,
                commodity_id: findSurvey.commodity_id,
                survey_date: findSurvey.survey_date,
                commodity_price: findSurvey.commodity_price,
                approved: 1,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            
            await commit(save, deactivate && create)
            return {status: 1}
        },
        reject: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            req.id = hashids.decode(req.id)
            let findSurvey = await db.survey.findOne({
                attributes: ['id', 'commodity_id', 'commodity_price', 'survey_date'],
                where: { id: req.id, approved: 0, surveyor_id: ctx.req.auth.username, statusid: 1 },
                raw: true
            })
            await validate(findSurvey, 'user-input')
            
            let save = await sequelize.transaction()
            let deactivate = await db.survey.update(
                { statusid: 0 },
                { where: { id: findSurvey.id, statusid: 1 } }
            )
            let create = await db.survey.create({
                surveyor_id: ctx.req.auth.username,
                commodity_id: findSurvey.commodity_id,
                survey_date: findSurvey.survey_date,
                commodity_price: findSurvey.commodity_price,
                approved: 2,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            
            await commit(save, deactivate && create)
            return {status: 1}
        }
    }
}