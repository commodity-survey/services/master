`use strict`
const { Validator: v } = require('node-input-validator')
const { hashids, validate } = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { commodity: () => ({}) },
    Mutation: { commodity: () => ({}) },
    CommodityQuery: {
        get: async () => {
            let commodity = await db.m_commodity.findAll({
                attributes: [
                    'id', 'commodity_name', 'category_id', 'unit_id', 
                    [sequelize.col('mu.unit_name'), 'unit_name'], 
                    [sequelize.col('mc.category_name'), 'category_name']
                ],
                include : [
                    { model : db.m_category, as:'mc', attributes : [], require : true },
                    { model : db.m_unit, as:'mu', attributes : [], require : true }
                ],
                where: { statusid: 1 },
                raw: true
            })
            commodity = hashids.encodeArray(commodity, ['id', 'category_id', 'unit_id'])
            return commodity
        }
    },
    CommodityMutation: {
        create: async (__, req, ctx) => {
            let validator = new v(req, { commodity_name: 'required', category_id: 'required', unit_id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            const commodity_name = req.commodity_name.toLowerCase()
            let validateName = await db.m_commodity.findOne({
                attributes: ['commodity_name'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('commodity_name')), 
                            sequelize.fn('lower', commodity_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')

            req.category_id = hashids.decode(req.category_id)
            req.unit_id = hashids.decode(req.unit_id)
            let validateCategory = await db.m_category.findOne({
                attributes: ['id'],
                where: { id: req.category_id, statusid: 1 },
                raw: true
            })
            await validate(validateCategory, 'user-input')

            let validateUnit = await db.m_unit.findOne({
                attributes: ['id'],
                where: { id: req.unit_id, statusid: 1 },
                raw: true
            })
            await validate(validateUnit, 'user-input')

            let save = await db.m_commodity.create({
                commodity_name: req.commodity_name,
                category_id: req.category_id,
                unit_id: req.unit_id,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async(__, req, ctx) => {
            let validator = new v(req, { id: 'required', commodity_name: 'required', category_id: 'required', unit_id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_commodity.findOne({
                attributes: ['id'],
                where: { id: req.id, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            const commodity_name = req.commodity_name.toLowerCase()
            let validateName = await db.m_commodity.findOne({
                attributes: ['commodity_name'],
                where: { 
                    [Op.and]: [
                        { id: { [Op.ne]: req.id } }, 
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('commodity_name')), 
                            sequelize.fn('lower', commodity_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')

            req.category_id = hashids.decode(req.category_id)
            req.unit_id = hashids.decode(req.unit_id)
            let validateCategory = await db.m_category.findOne({
                attributes: ['id'],
                where: { id: req.category_id, statusid: 1 },
                raw: true
            })
            await validate(validateCategory, 'user-input')

            let validateUnit = await db.m_unit.findOne({
                attributes: ['id'],
                where: { id: req.unit_id, statusid: 1 },
                raw: true
            })
            await validate(validateUnit, 'user-input')
            
            let save = db.m_commodity.update(
                { 
                    commodity_name: req.commodity_name,
                    category_id: req.category_id,
                    unit_id: req.unit_id
                },
                { where: { id: req.id, statusid: 1 } }
            )

            await validate(save)
            return { id: hashids.encode(save.id) }
        },
        delete: async (__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_commodity.findOne({
                attributes: ['id'],
                where: { id: req.id, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            let save = db.m_commodity.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            
            return {status: 1}
        }
    }
}