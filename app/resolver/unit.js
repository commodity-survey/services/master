`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { unit: () => ({}) },
    Mutation: { unit: () => ({}) },
    UnitQuery: {
        get: async () => {
            let unit = await db.m_unit.findAll({
                attributes: ['id', 'unit_name' ],
                where: { statusid: 1 },
                order: ['unit_name'],
                raw: true
            })
            unit = hashids.encodeArray(unit, 'id')
            return unit
        }
    },
    UnitMutation: {
        create: async (__, req) => {
            let validator = new v(req, { unit_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            const unit_name = req.unit_name.toLowerCase()
            let validateName = await db.m_unit.findOne({
                attributes: ['unit_name'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('unit_name')), 
                            sequelize.fn('lower', unit_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')

            let save = await db.m_unit.create({
                unit_name: req.unit_name,
                statusid: 1
            })
            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async (__, req) => {
            let validator = new v(req, { id: 'required', unit_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_unit.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')

            const unit_name = req.unit_name.toLowerCase()
            let validateName = await db.m_unit.findOne({
                attributes: ['unit_name'],
                where: { 
                    [Op.and]: [
                        { id: { [Op.ne]: req.id } }, 
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('unit_name')), 
                            sequelize.fn('lower', unit_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')
            
            let save = db.m_unit.update(
                { unit_name: req.unit_name },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        },
        delete: async (__, req) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_unit.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')

            let save = db.m_unit.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        }
    }
}