`use strict`
const { Validator: v } = require('node-input-validator')
const {hashids, validate} = require(`${__basedir}/app/helper/helper`)

module.exports = {
    Query: { category: () => ({}) },
    Mutation: { category: () => ({}) },
    CategoryQuery: {
        get: async () => {
            let category = await db.m_category.findAll({
                attributes: ['id', 'category_name' ],
                where: { statusid: 1 },
                order: ['category_name'],
                raw: true
            })
            category = hashids.encodeArray(category, 'id')
            return category
        }
    },
    CategoryMutation: {
        create: async (__, req) => {
            let validator = new v(req, { category_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            const category_name = req.category_name.toLowerCase()
            let validateName = await db.m_category.findOne({
                attributes: ['category_name'],
                where: { 
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('category_name')), 
                            sequelize.fn('lower', category_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')


            let save = await db.m_category.create({
                category_name: req.category_name,
                statusid: 1
            })

            await validate(save)
            return {id: hashids.encode(save.id)}
        },
        update: async (__, req) => {
            let validator = new v(req, { id: 'required', category_name: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_category.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')
            
            const category_name = req.category_name.toLowerCase()
            let validateName = await db.m_category.findOne({
                attributes: ['category_name'],
                where: { 
                    [Op.and]: [
                        { id: { [Op.ne]: req.id } }, 
                        sequelize.where(
                            sequelize.fn('lower', sequelize.col('category_name')), 
                            sequelize.fn('lower', category_name)
                        ),
                        { statusid: 1 } 
                    ]
                },
                raw: true
            })
            await validate(!validateName, 'user-input')
            
            let save = db.m_category.update(
                { category_name: req.category_name },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        },
        delete: async (__, req) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            req.id = hashids.decode(req.id)
            let find = await db.m_category.findOne({ attributes: ['id'], where: { id: req.id, statusid: 1 }, raw: true })
            await validate(find, 'user-input')

            let save = db.m_category.update(
                { statusid: 0 },
                { where: { id: req.id, statusid: 1 } }
            )
            await validate(save)
            return { status: 1 }
        }
    }
}