`use strict`
const bcrypt = require('bcryptjs')
const { Validator: v } = require('node-input-validator')
const { validate, commit, files, random } = require(`${__basedir}/app/helper/helper`)
const axios = require('axios')
const FormData = require('form-data')
const fs = require('fs')

module.exports = {
    Query: { account: () => ({}) },
    Mutation: { account: () => ({}) },
    AccountQuery: {
        get: async (__, req, ctx) => {
            let account = await db.authentication_user.findAll({
                attributes: [
                    'username', [sequelize.col('spd.name'), 'name'],
                    [sequelize.col('spd.address'), 'address'],
                ],
                include : [{ model : db.s_personal_data, as: 'spd', attributes : [], where: {appid: ctx.req.auth.app.id, statusid: 1}, require : true }],
                where: { appid: ctx.req.auth.app.id, statusid: 1 },
                raw: true
            })
            
            let userRoles = await db.authentication_user_role.findAll({
                attributes: [ 'username', 'rolename', [sequelize.col('ar.roledesc'), 'roledesc'] ],
                include: [{ model: db.authentication_role, as: 'ar', attributes: [], where: {appid: ctx.req.auth.app.id, statusid: 1}, require: true }],
                where: { appid: ctx.req.auth.app.id, statusid: 1 },
                raw: true
            })

            for (index in account) {
                let row = account[index]
                account[index].roles = []
                for(let nestedRow of userRoles) {
                    if(row.username == nestedRow.username) {
                        account[index].roles.push({
                            rolename: nestedRow.rolename,
                            roledesc: nestedRow.roledesc
                        })
                    }
                }
            }

            return account
        },
        view: async(__, req, ctx) => {
            let validator = new v(req, { id: 'required' })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let account = await db.authentication_user.findOne({
                attributes: [
                    'username',
                    [sequelize.col('spd.name'), 'name'],
                    [sequelize.col('spd.address'), 'address'],
                    [sequelize.col('spd.photo'), 'photo'],
                ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: req.id, statusid: 1 },
                raw: true
            })
            await validate(account, 'user-input')
            
            account.roles = await db.authentication_user_role.findAll({
                attributes: [ 'rolename', [sequelize.col('ar.roledesc'), 'roledesc'] ],
                include: [{ model: db.authentication_role, as: 'ar', attributes: [], where: { appid: ctx.req.auth.app.id, statusid: 1}, require: true }],
                where: { appid: ctx.req.auth.app.id, username: req.id, statusid: 1 },
                raw: true
            })

            account.hasPhoto = account.photo ? true : false
            return account
        },
        roles: async (__, req, ctx) => {
            let roles = await db.authentication_role.findAll({ 
                attributes: [ 'rolename', 'roledesc' ], 
                where: { appid: ctx.req.auth.app.id, statusid: 1 }, 
                raw: true 
            })
            return roles
        }
    },
    AccountMutation: {
        create: async (__, req, ctx) => {
            let validator = new v(req, { 
                name: 'required',
                username: 'required',
                password: 'required|same:repassword',
                roles: 'required|array',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')
            
            for(let row of req.roles) {
                let findRoles = await db.authentication_role.findOne({ 
                    attributes: ['id'], 
                    where: [{appid: ctx.req.auth.app.id, rolename: row, statusid: 1}], 
                    raw: true 
                })
                await validate(findRoles, 'user-input')
            }
            
            let findUsername = await db.authentication_user.findOne({ 
                attributes: ['id'], 
                where: {appid: ctx.req.auth.app.id, username: req.username, statusid: 1}, 
                raw: true 
            })
            await validate(!findUsername, 'user-input')
            
            const password = await bcrypt.hash(req.password, env.app.secret)
            
            let save = await sequelize.transaction()
            let saveUser = await db.authentication_user.create({
                appid: ctx.req.auth.app.id, 
                username: req.username,
                password: password,
                create_at: sequelize.fn('NOW'),
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            let savePersonalData = await db.s_personal_data.create({
                appid: ctx.req.auth.app.id, 
                username: req.username,
                name: req.name,
                create_at: sequelize.fn('NOW'),
                create_by: ctx.req.auth.username,
                statusid: 1
            })
    
            let userRolesData = []
            for(let row of req.roles) {
                userRolesData.push({
                    appid: ctx.req.auth.app.id, 
                    username: req.username,
                    rolename: row,
                    create_at: sequelize.fn('NOW'),
                    create_by: ctx.req.auth.username,
                    statusid: 1
                })
            }

            let saveRoles = await db.authentication_user_role.bulkCreate(userRolesData)
            
            await commit(save, saveUser && savePersonalData && saveRoles)
            return {status: 1}
        },
        savePersonalData: async(__, req, ctx) => {
            let validator = new v(req, { 
                username: 'required',
                name: 'required',
                address: 'required',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ [sequelize.col('spd.id'), 'id'], [sequelize.col('spd.photo'), 'photo'] ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: req.username, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')
            
            let save = await sequelize.transaction()

            let deactivePersonalData = await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id } })
            let savePersonalData = await db.s_personal_data.create({
                appid: ctx.req.auth.app.id,
                username: req.username,
                name: req.name,
                address: req.address,
                photo: find.photo,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            
            await commit(save, deactivePersonalData && savePersonalData)
            return {status: 1}
        },
        saveRoles: async (__, req, ctx) => {
            let validator = new v(req, { 
                username: 'required',
                roles: 'required',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ [sequelize.col('spd.id'), 'id'] ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: req.username, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            let userRolesData = []
            for(let row of req.roles) {
                userRolesData.push({
                    appid: ctx.req.auth.app.id, 
                    username: req.username,
                    rolename: row,
                    create_at: sequelize.fn('NOW'),
                    create_by: ctx.req.auth.username,
                    statusid: 1
                })
            }
            
            let save = await sequelize.transaction()
            let deactiveRoles = await db.authentication_user_role.update({ statusid: 0 }, { where: { 
                appid: ctx.req.auth.app.id, 
                username: req.username 
            } })
            let saveRoles = await db.authentication_user_role.bulkCreate(userRolesData)
            
            await commit(save, deactiveRoles && saveRoles)
            return { status: 1 }
        },
        savePassword: async (__, req, ctx) => {
            let validator = new v(req, { 
                username: 'required',
                password: 'required|same:repassword',
            })
            let matched = await validator.check()
            await validate(matched, 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ 'id' ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: req.username, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            const password = await bcrypt.hash(req.password, env.app.secret)
            
            let save = await sequelize.transaction()
            let deactiveUser = await db.authentication_user.update({ statusid: 0 }, { where: { id: find.id } })
            let saveUser = await db.authentication_user.create({
                appid: ctx.req.auth.app.id, 
                username: req.username,
                password: password,
                create_by: ctx.req.auth.username,
                statusid: 1
            })
            await commit(save, deactiveUser && saveUser)
            return { status: 1 }
        },
        savePicture: async (_, req, ctx) => {
            let validator = new v(req, { 
                id: 'required',
                picture: 'required|array',
            })
            await validate(await validator.check(), 'user-input')

            const { filename, createReadStream } = await req.picture[0]
            
            const picture = createReadStream({options: { flags: 'r', encoding: null}})
            let fileValidator = new v({picture}, { 
                picture: 'required|mime:jpg,png',
            })
            await validate(await fileValidator.check(), 'user-input')

            let find = await db.authentication_user.findOne({
                attributes: [ 
                    [sequelize.col('spd.id'), 'id'], 
                    [sequelize.col('spd.name'), 'name'],
                    [sequelize.col('spd.address'), 'address'],
                ],
                include: [{ 
                    model: db.s_personal_data, as: 'spd', attributes: [], where: {
                        appid: ctx.req.auth.app.id, 
                        statusid: 1
                    } 
                }],
                where: { appid: ctx.req.auth.app.id, username: req.id, statusid: 1 },
                raw: true
            })
            await validate(find, 'user-input')

            const splittedFilename = filename.split('.')
            const convertedFilename = `${random(50)}.${splittedFilename[splittedFilename.length-1]}`
            const file = files.graphqlConvertStream(convertedFilename, picture)
            
            const form = new FormData()
            form.append('picture', file)
            form.append('filename', convertedFilename )
            axios.post(`/file/save-display-picture`, form, {headers: form.getHeaders()}).then(async response => {
                await validate(response.data.status)

                let save = await sequelize.transaction()
                
                let deactivePersonalData = await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id } })
                let savePersonalData = await db.s_personal_data.create({
                    appid: ctx.req.auth.app.id,
                    username: req.id,
                    name: find.name,
                    address: find.address,
                    photo: convertedFilename,
                    create_by: ctx.req.auth.username,
                    statusid: 1
                })
                
                await commit(save, deactivePersonalData && savePersonalData)
                return {status: 1}
            }).catch(e => {
                console.log(e.message)
                validate(false)
            })
        },
        delete: async (__, req, ctx) => {

        }
    }
}