'use strict'
module.exports = () => {
    /** Menu & Label */
    db.s_label_menu.hasMany(db.s_menu, {foreignKey : 'id'})
    db.s_menu.belongsTo(db.s_label_menu, {foreignKey : 'label_id'})

    /** Master Data */
    db.m_commodity.belongsTo(db.m_category, { as: 'mc', foreignKey : 'category_id'})
    db.m_commodity.belongsTo(db.m_unit, { as: 'mu', foreignKey : 'unit_id'})
    db.m_category.hasMany(db.m_commodity, { as: 'mcom', foreignKey : 'id'})
    db.m_unit.hasMany(db.m_commodity, { as: 'mcom', foreignKey : 'id'})
    
    /** Survey */
    db.survey.belongsTo(db.m_commodity, { as: 'mcom', foreignKey : 'commodity_id'})
    db.m_commodity.hasMany(db.survey, { as: 's', foreignKey : 'id'})

    /** User & Authentication */
    db.authentication_user.hasOne(db.s_personal_data, {as: 'spd', foreignKey : 'username', sourceKey : 'username'})
    db.s_personal_data.belongsTo(db.authentication_user, {as: 'au', foreignKey : 'username'})

    /** Roles */
    db.authentication_role.hasMany(db.authentication_user_role, { as: 'aur', foreignKey : 'rolename'})
    db.authentication_user_role.belongsTo(db.authentication_role, { as: 'ar', foreignKey : 'rolename', targetKey: 'rolename'})
}
