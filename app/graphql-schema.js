const { gql } = require('apollo-server-express')

const main = gql`
    scalar JSON
    scalar JSONObject
    scalar Date
    scalar DateTime
    type AccountQuery {
        get: [JSONObject]
        roles: [JSONObject]
        view(id: String!): JSONObject
    }
    type AccountMutation {
        create(username: String!, name: String!, address: String!, password: String!, repassword: String!, roles: [String]!): JSONObject
        savePersonalData(username: String!, name: String!, address: String!): JSONObject
        savePassword(username: String!, password: String!, repassword: String!): JSONObject
        saveRoles(username: String!, roles: [String]!): JSONObject
        savePicture(id: String!, picture: [Upload!]!): JSONObject
        delete(id: String!): JSONObject
    }
    type CategoryQuery {
        get: [JSONObject]
    }
    type CategoryMutation {
        create(category_name: String!): JSONObject
        update(id: String!, category_name: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type UnitQuery {
        get: [JSONObject]
    }
    type UnitMutation {
        create(unit_name: String!): JSONObject
        update(id: String!, unit_name: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type CommodityQuery {
        get: [JSONObject]
    }
    type CommodityMutation {
        create(commodity_name: String!, category_id: String!, unit_id: String!): JSONObject
        update(id: String!, commodity_name: String!, category_id: String!, unit_id: String!): JSONObject
        delete(id: String!): JSONObject
    }
    type getForApprovalOutput {
        approved: [JSONObject]
        rejected: [JSONObject]
        waiting: [JSONObject]
    }
    type SurveyorQuery {
        getForSurveyor(filter_date_min: String, filter_date_max: String, commodity: [String], category: [String]): [JSONObject]
    }
    type SurveyorApprovalQuery {
        getForApproval: getForApprovalOutput
    }
    type SurveyMutation {
        create(survey_date: String!, commodity_id: String!, commodity_price: Int!): JSONObject
        update(id: String!, survey_date: String!, commodity_id: String!, commodity_price: Int!): JSONObject
        delete(id: String!): JSONObject
        approve(id: String!): JSONObject
        reject(id: String!): JSONObject
    }
    type SurveyForVisitorQuery {
        get(filter_date_min: String, filter_date_max: String) : [JSONObject]
    }
    type Query {
        userinfo: JSONObject
        account: AccountQuery
        category: CategoryQuery
        unit: UnitQuery
        commodity: CommodityQuery
        surveyor: SurveyorQuery
        approval: SurveyorApprovalQuery
        visitor: SurveyForVisitorQuery
    }
    type Mutation {
        authenticate(username: String!, password: String!): JSONObject
        account: AccountMutation
        category: CategoryMutation
        unit: UnitMutation
        commodity: CommodityMutation
        survey: SurveyMutation
    }
`

module.exports = [main]