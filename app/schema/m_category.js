`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`m_category`, {
        category_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'masterdata',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    })
}