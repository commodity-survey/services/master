`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`m_commodity`, {
        category_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        unit_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        commodity_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'masterdata',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    })
}