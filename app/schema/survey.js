`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`survey`, {
        surveyor_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        commodity_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        commodity_price : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        survey_date : {
            type      : Sequelize.DATE,
            allowNull : false
        },
        approved : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            defaultValue: Sequelize.fn('NOW'),
            allowNull : false
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'transactional',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    })
}