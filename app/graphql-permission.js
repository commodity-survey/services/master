const { rule, shield, and, or } = require('graphql-shield')
const authenticated = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth ? true : false
    },
)
const administrator = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth.roles.includes('administrator')
    },
)
const surveyor = rule({ cache: 'contextual' })(
    async (parent, args, {req}, info) => {
        return req.auth.roles.includes('super')
    },
)
module.exports = shield({
    Query: {
        userinfo: authenticated,
        category: and(authenticated, administrator),
        account: and(authenticated, administrator),
        unit: and(authenticated, administrator),
        commodity: and(authenticated, administrator),
        surveyor: and(authenticated, or(administrator, surveyor)),
        approval: and(authenticated, administrator),
    },
    Mutation: {
        category: and(authenticated, administrator),
        account: and(authenticated, administrator),
        category: and(authenticated, administrator),
        unit: and(authenticated, administrator),
        commodity: and(authenticated, administrator),
        survey: {
            create: and(authenticated, or(administrator, surveyor)),
            update: and(authenticated, or(administrator, surveyor)),
            delete: and(authenticated, or(administrator, surveyor)),
            approve: and(authenticated, administrator),
            reject: and(authenticated, administrator),
        }
    }
},{
    fallbackError: true,
    debug: true
})