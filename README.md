# Master

<h4 style="font-weight: bold">Administrator Account: </h4>
<ul>
    <li> username: administrator </li>
    <li> password: september09 </li>
    <li> encryption password: bcrypt </li>
    <li> authorization token: jwt </li>
</ul>

<h4 style="font-weight: bold">Surveyor Account: </h4>
<ul>
    <li> username: surveyor </li>
    <li> password: september09 </li>
    <li> encryption password: bcrypt </li>
    <li> authorization token: jwt </li>
</ul>

<h4 style="font-weight: bold">Needs: </h4>
<ol>
    <li>Node JS</li>
    <li>Package Manager (such as NPM)</li>
    <li>PM2</li>
    <li>Internet Connection</li>
    <li>Postgres</li>
    <li>Proxy Pass (Nginx)</li>
</ol>

<h4 style="font-weight: bold">Installation Steps: </h4>

1. Import database (Feel free to send me a message when you are interested with my project)
2. Clone this project from Gitlab
3. Install Package for NodeJS "npm i"
4. Setup Proxy Pass. In my case, I use nginx as proxy pass, here is my code:
    <pre>
        <code>
            location /commodity-survey-api/graphql {
                proxy_pass "http://localhost:9632/graphql";
            }
        </code>
    </pre>
5. Run this project from cli using "npm start"
6. Don't forget to restart your web server/proxy pass after you make some changes