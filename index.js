const express = require('express')
const {ApolloServer } = require('apollo-server-express')

global.env = require(`${__dirname}/config/env.json`)
global.__basedir = __dirname

const db = require(`${__dirname}/core/db`)
const schemaRelation = require(`${__dirname}/app/schema-relation`)
const cors = require('cors')
const typeDefs = require('./app/graphql-schema')
const resolvers = require('./app/resolver')
const authorize = require('./app/middleware/authorization')
const permission = require('./app/graphql-permission')
const { applyMiddleware } = require('graphql-middleware')
const axios = require('axios')

const app = {
    start() {
        return new Promise(async (resolve, reject) => { resolve({ db : await db() }) }).then(() => {
            const app = new express()
            const server = new ApolloServer({ typeDefs, resolvers, context: authorize })
            server.schema = applyMiddleware(server.schema, permission)
            
            app.use(cors())
            server.applyMiddleware({app})
            
            app.listen(env.app.port, env.app.host)
            
            axios.defaults.headers.common['authorization'] = env.app.secret
            axios.defaults.baseURL = `http://${env.app.host}/${env.app.url}/`
            axios.defaults.headers.post['Content-Type'] = 'multipart/form-data'
            
            schemaRelation()
            return server
        })
    },
}

app.start()